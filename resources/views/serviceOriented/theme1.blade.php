@extends('layouts.main')

@section('body-class')
    services-page
@endsection

@section('main-content')

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner-page-banner wow fadeIn">
        <div class="container">
            <h4>Brand Name</h4>
            <ul class="breadcrumbs">
                <li><a href="index-2.html">Home</a></li>
                <li>/</li>
                <li>Services</li>
                <li>/</li>
                <li class="active">{Service Provider Name}</li>
            </ul>
        </div>
    </div>

    <section class="section lb bottom-grass wow fadeIn">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-hidden col-lg-hidden">
                        <div class="lady-garden">
                            <img src="images/01-1.png" class="img-responsive" alt="#" />
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-hidden col-lg-hidden welcome-about-text" style="margin-top:15px;">
                        <h3>Welcome to Grasscare</h3>
                        <p>There are many variations of passages of Lorem Ipsum available,
                            but the majority have suffered alteration in some form, by injected humour,
                            or randomised words which don't look even slightly believable.</p>

                        <div class="col-md-12" style="margin-top:25px;">
                            <div class="row">
                                <div class="col-md-6 info-about">
                                    <div class="icon-information">
                                        <img src="images/icon1.png" alt="#" />
                                    </div>
                                    <div class="icon-text">
                                        <h3>Who we are</h3>
                                        <p>The readable content of a page when looking at its layout.<p>
                                    </div>
                                </div>
                                <div class="col-md-6 info-about">
                                    <div class="icon-information">
                                        <img src="images/icon2.png" alt="#" />
                                    </div>
                                    <div class="icon-text">
                                        <h3>Our Mission</h3>
                                        <p>The readable content of a page when looking at its layout.<p>
                                    </div>
                                </div>
                                <div class="col-md-6 info-about">
                                    <div class="icon-information">
                                        <img src="images/icon3.png" alt="#" />
                                    </div>
                                    <div class="icon-text">
                                        <h3>Our Vision</h3>
                                        <p>The readable content of a page when looking at its layout.<p>
                                    </div>
                                </div>
                                <div class="col-md-6 info-about">
                                    <div class="icon-information">
                                        <img src="images/icon4.png" alt="#" />
                                    </div>
                                    <div class="icon-text">
                                        <h3>Our Credits</h3>
                                        <p>The readable content of a page when looking at its layout.<p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="section wow fadeIn">
        <div class="container">
            <div class="section-title text-center">
                <h3>Why Us?</h3>
                <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <!-- end title -->
            <div class="row">
                <div class="col-md-4">
                    <div class="service-render">

                        <div class="service-render-text">
                            <h4 style="background:#8ad51c;">Hedge Trimming</h4>
                        </div>

                        <div class="feature-img">
                            <img src="images/img-cap4.jpg" class="img-responsive" alt="" />
                            <div class="feature-text-hover">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at.</div>
                        </div>

                    </div>
                </div>
                <!-- end col -->
                <div class="col-md-4">
                    <div class="service-render">

                        <div class="service-render-text">
                            <h4 style="background:#74b11b;">Grass sod and Turf Laying</h4>
                        </div>

                        <div class="feature-img">
                            <img src="images/img-cap5.jpg" class="img-responsive" alt="" />
                            <div class="feature-text-hover">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at.</div>
                        </div>

                    </div>
                </div>
                <!-- end col -->
                <div class="col-md-4">
                    <div class="service-render">

                        <div class="service-render-text">
                            <h4 style="background:#5c9011;">Saplings and Seeds</h4>
                        </div>

                        <div class="feature-img">
                            <img src="images/img-cap6.jpg" class="img-responsive" alt="" />
                            <div class="feature-text-hover">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at.</div>
                        </div>

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <div class="section lb brwon-img wow fadeIn">
        <div class="container">
            <div class="section-title text-center">
                <h3>What We Do</h3>
                <p class="lead">Listed below our services</p>
            </div>
            <!-- end title -->
            <div class="section-widget text-center">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="hoverbox parallax parallax-off" data-stellar-background-ratio="1" style="background-image:url('images/service_01.jpg');">
                            <a href="page-services.html" class="box">
                                <div class="box-content">
                                    <i class="flaticon-forest"></i>
                                    <h2><span>Afforestation</span></h2>
                                </div>
                                <div class="box-back">
                                    <p>Morbi gravida nisl ac purus vulputate, ac convallis tellus dapibus.</p>
                                </div>
                            </a>
                        </div>
                        <!-- end hover-box -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="hoverbox parallax parallax-off" data-stellar-background-ratio="1" style="background-image:url('images/service_02.jpg');">
                            <a href="page-services.html" class="box">
                                <div class="box-content">
                                    <i class="flaticon-fence"></i>
                                    <h2><span>Garden Fence</span></h2>
                                </div>
                                <div class="box-back">
                                    <p>Nam malesuada sem in lectus tincidunt, vel interdum ex tincidunt.</p>
                                </div>
                            </a>
                        </div>
                        <!-- end hover-box -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="hoverbox parallax parallax-off" data-stellar-background-ratio="1" style="background-image:url('images/service_03.jpg');">
                            <a href="page-services.html" class="box">
                                <div class="box-content">
                                    <i class="flaticon-house"></i>
                                    <h2><span>House Gardening</span></h2>
                                </div>
                                <div class="box-back">
                                    <p>Vivamus viverra lectus vel nulla mattis, fringilla mattis rhoncus.</p>
                                </div>
                            </a>
                        </div>
                        <!-- end hover-box -->
                    </div>
                    <!-- end col -->
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn">
                        <div class="hoverbox parallax parallax-off" data-stellar-background-ratio="1" style="background-image:url('images/service_04.jpg');">
                            <a href="page-services.html" class="box">
                                <div class="box-content">
                                    <i class="flaticon-shovel-1"></i>
                                    <h2><span>Garden Supplies</span></h2>
                                </div>
                                <div class="box-back">
                                    <p>Aliquam eu leo blandit, faucibus neque sed, varius urna.</p>
                                </div>
                            </a>
                        </div>
                        <!-- end hover-box -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end section-widget -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <div class="section lb our-gar wow fadeIn">
        <div class="container">
            <div class="section-title text-center">
                <h3>Our Gardeners</h3>
                <p class="lead">Meet professional gardeners on the market</p>
            </div>
            <!-- end title -->
            <div class="row team-members">
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_01.jpg" alt="#" class="img-responsive" />
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>CEO / Founder</small>
                            <h4>Martin Martines</h4>
                        </div>

                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_02.jpg" alt="" class="img-responsive" />
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>CEO / Founder</small>
                            <h4>Linda Martines</h4>
                        </div>

                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-4 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/team_03.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-facebook"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-twitter"></span></a>
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-google-plus"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>Pro Gardener</small>
                            <h4>Semanta Doe</h4>
                        </div>

                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <div class="section lb brwon-img wow fadeIn">
        <div class="container">
            <div class="section-title text-center">
                <h3>Our Pricing</h3>
                <p class="lead">Listed below our plans & pricings</p>
            </div>
            <!-- end title -->
            <div class="row">
                <div class="col-md-4 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="pricing-box-03 text-right">
                        <div class="pricing-box-03-head">
                            <h3><sup>$</sup>55.00</h3>
                            <h4>Beginner Package</h4>
                        </div>
                        <!-- end pricing-box-09-head -->
                        <div class="pricing-box-03-body">
                            <ul>
                                <li>Garden Supplies</li>
                                <li>Garden Machines</li>
                                <li>Garden Arrangement</li>
                                <li>Professional Staff</li>
                                <li>Lifetime Support</li>
                                <li>Important Changes</li>
                            </ul>
                        </div>
                        <!-- end pricing-box-06-body -->
                        <div class="pricing-box-03-foot">
                            <a href="#" class="btn btn-primary">Order Now</a>
                        </div>
                        <!-- end pricing-box-03-foot -->
                    </div>
                    <!-- end pricing-box -->
                </div>
                <!-- end col -->
                <div class="col-md-4 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                    <div class="pricing-box-03 text-center nobg">
                        <div class="pricing-box-03-head">
                            <h3><sup>$</sup>90.00</h3>
                            <h4>Business Package</h4>
                        </div>
                        <!-- end pricing-box-09-head -->
                        <div class="pricing-box-03-body">
                            <ul>
                                <li>Garden Supplies</li>
                                <li>Garden Machines</li>
                                <li>Garden Arrangement</li>
                                <li>Professional Staff</li>
                                <li>Lifetime Support</li>
                                <li>Important Changes</li>
                            </ul>
                        </div>
                        <!-- end pricing-box-06-body -->
                        <div class="pricing-box-03-foot">
                            <a href="#" class="btn btn-primary">Order Now</a>
                        </div>
                        <!-- end pricing-box-03-foot -->
                    </div>
                    <!-- end pricing-box -->
                </div>
                <!-- end col -->
                <div class="col-md-4 col-sm-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">
                    <div class="pricing-box-03 leftbg text-left">
                        <div class="pricing-box-03-head">
                            <h3><sup>$</sup>120.00</h3>
                            <h4>Professional Package</h4>
                        </div>
                        <!-- end pricing-box-09-head -->
                        <div class="pricing-box-03-body">
                            <ul>
                                <li>Garden Supplies</li>
                                <li>Garden Machines</li>
                                <li>Garden Arrangement</li>
                                <li>Professional Staff</li>
                                <li>Lifetime Support</li>
                                <li>Important Changes</li>
                            </ul>
                        </div>
                        <!-- end pricing-box-06-body -->
                        <div class="pricing-box-03-foot">
                            <a href="#" class="btn btn-primary">Order Now</a>
                        </div>
                        <!-- end pricing-box-03-foot -->
                    </div>
                    <!-- end pricing-box -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
    <div class="section lb blog wow fadeIn">
        <div class="container">
            <div class="section-title">
                <i class="flaticon-link"></i>
                <h3>From the Blog</h3>
                <p class="lead">Learn garden, landscaping with our blog posts</p>
            </div>
            <!-- end title -->
            <div class="row blog-wrapper">
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_02.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Semanta</a></small>
                            <h4><a href="single.html">The most authentic garden supplies</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nunc augue purus, posuere in accumsan sodales ac, euismod at est. Nunc faccumsan ermentum consectetur metus placerat mattis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_03.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Bob</a></small>
                            <h4><a href="single.html">Top quality materials gardener</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Nam vitae imperdiet turpis. Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end col -->
                <div class="col-md-6 wow fadeIn">
                    <div class="about-widget">
                        <div class="post-media entry">
                            <img src="images/blog_01.jpg" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="#"><span class="fa fa-link"></span></a>
                                </div>
                                <!-- end buttons -->
                            </div>
                            <!-- end magnifier -->
                        </div>
                        <!-- end media -->
                        <div class="small-title">
                            <small>21/12/2016</small>
                            <small><a href="#">by Martines</a></small>
                            <h4><a href="single.html">benches made for gardens</a></h4>
                        </div>
                        <!-- end small-title -->
                        <p>Praesent mollis justo felis, accumsan faucibus mi maximus et. Nam hendrerit mauris id scelerisque placerat. Nam vitae imperdiet turpis.</p>
                        <a class="readmore" href="single.html">Read More</a>
                    </div>
                    <!-- end about-widget -->
                </div>
                <!-- end blog -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    {{--Map--}}
    <div id="map">
    </div>
    {{--End map--}}
@endsection

@section('custom-script')
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <!-- MAP & CONTACT -->
    <script src="{{asset('js/map.js')}}"></script>
@endsection
