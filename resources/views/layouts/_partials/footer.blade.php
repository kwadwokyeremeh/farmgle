<footer class="gardener wow fadeIn">
    <div class="container">
        <div class="row">
            <div id="bunch_about_us-2" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 widget footer-widget widget_bunch_about_us">
                <div class="">
                    <h3>About Us</h3>
                    <p>Lorem ipsum dolor sit amet, consect etur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. Ut enim.</p>
                    <a href="#" class="read-more">Read More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- /.widget -->
            </div>
            <div id="bunch_services_posts-2" class="col-lg-2 col-md-3 col-sm-6 col-xs-12 widget footer-widget widget_bunch_services_posts">
                <div class="part2">
                    <h3>Our Services</h3>
                    <!-- Title -->
                    <ul class="quick-links">
                        <li><a href="#">Watering Garden</a></li>
                        <li><a href="#">Lawn Removing</a></li>
                        <li><a href="#">Garden Care</a></li>
                        <li><a href="#">Landscape Design</a></li>
                        <li><a href="#">Rubbish Removal</a></li>
                    </ul>
                </div>
            </div>
            <div id="bunch_get_in_touch-2" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 widget footer-widget widget_bunch_get_in_touch">
                <div class="widget">
                    <h3>Get in Touch</h3>
                    <ul class="contact-info">
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="content">
                                <p>Lorem ipsum dolor sit amet, consect etur adipiscing elit</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="content phone">
                                <p>01865 524 8503</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-box">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="content">
                                <p>contact@example.com</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <div class="col-md-3">
                <div class="widget">
                    <h3>Appointment Now</h3>
                    <div class="workinghours">
                        <ul>
                            <li><i class="fa fa-clock-o"></i> Weekdays <span>9:00am - 7:00pm</span></li>
                            <li><i class="fa fa-clock-o"></i> Weekend <span>9:00am - 7:00pm</span></li>
                            <li><i class="fa fa-phone"></i> Phone <span>+90 534 970 00 00</span></li>
                            <li><i class="fa fa-fax"></i> Fax <span>+90 534 970 00 01</span></li>
                            <li><i class="fa fa-envelope-o"></i> E-Mail <span>support@yoursite.com</span></li>
                            <li><i class="fa fa-link"></i> Website <span>yoursite.com</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="bunch_footer_image-2" class="col-lg-3 col-md-4 col-sm-6 col-xs-12 widget footer-widget widget_bunch_footer_image">
                <!-- .widget -->
                <div class="hidden-md">
                    <img class="positioned" src="images/footer-man.png" alt="" style="visibility: visible; animation-name: slideInRight;">
                </div>
                <!-- /.widget -->
            </div>
            <div class="footer-bottom">
                <div class="col-sm-4">
                    <p>© Grasscare INC. All rights reserved 2016.</p>
                </div>
                <div class="col-sm-4">
                    <ul class="list-inline">
                        <li><a href="index-2.html">Home</a></li>
                        <li><a href="#">Terms of Usage</a></li>
                        <li><a href="page-contact.html">Contact</a></li>
                        <li class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
