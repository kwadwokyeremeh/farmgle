@extends('layouts.main')
@section('body-class')
shopping-cart
@endsection


@section('main-content')

    <div class="section product-img-main bottom-grass wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="product-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Total</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="col-sm-8 col-md-6">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="#"> <img class="media-object" src="images/product-main1.jpg" alt="#" /></a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#">Flower Pots</a></h4>
                                            <span>Status: </span><span class="text-success">In Stock</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-sm-1 col-md-1" style="text-align: center">
                                    <input type="email" class="form-control" id="Quantity1" value="3">
                                </td>
                                <td class="col-sm-1 col-md-1 text-center">$21.00</td>
                                <td class="col-sm-1 col-md-1 text-center">$21.00</td>
                                <td class="col-sm-1 col-md-1">
                                    <button type="button" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span> Remove
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-6">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="#"> <img class="media-object" src="images/product-main2.jpg" alt="#" /></a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#">Flower Pots</a></h4>
                                            <span>Status: </span><span class="text-warning">Leaves warehouse in 2 - 3 weeks</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-md-1" style="text-align: center">
                                    <input type="email" class="form-control" id="Quantity2" value="2">
                                </td>
                                <td class="col-md-1 text-center">$21.00</td>
                                <td class="col-md-1 text-center">$21.00</td>
                                <td class="col-md-1">
                                    <button type="button" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span> Remove
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <tbody>
                            <tr class="cart-form">
                                <td class="actions">
                                    <div class="coupon">
                                        <input type="text" name="coupon_code" class="input-text" id="coupon_code" placeholder="Coupon code">
                                        <input type="submit" class="button" name="apply_coupon" value="Apply coupon">
                                    </div>
                                    <input type="submit" class="button" name="update_cart" value="Update cart" disabled="" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="shopping-cart-cart">
                        <table>
                            <tbody>
                            <tr class="head-table">
                                <td>
                                    <h5>Cart Totals</h5>
                                </td>
                                <td class="text-right"></td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>Subtotal</h4>
                                </td>
                                <td class="text-right">
                                    <h4>$42.00</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Estimated shipping</h5>
                                </td>
                                <td class="text-right">
                                    <h4>$6.00</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h3>Total</h3>
                                </td>
                                <td class="text-right">
                                    <h4>$48.00</h4>
                                </td>
                            </tr>
                            <tr>
                                <td><button type="button" class="button">  <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="button">
                                        Checkout <span class="glyphicon glyphicon-play"></span>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
@endsection

@section('custom-script')


@endsection
