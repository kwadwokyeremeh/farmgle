@extends('layouts.main')

@section('body-class')
    shopping-cart checkout
@endsection
@section('main-content')


    <div class="section bottom-grass wow fadeIn">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2>TELL US A BIT ABOUT YOUR PROFESSION</h2>
                    </div>

                    <div class="checkout-form">
                <form action="{{ route('wizard.freelancer.post', [$step::$slug]) }}" method="POST">
                    @csrf

                    @include($step::$view, compact('step', 'errors'))

                    @if ($wizard->hasPrev())
                        <a href="{{ route('wizard.user', ['step' => $wizard->prevSlug()]) }}">Back</a>
                    @endif

                    <span>Step {{ $step->number }}/{{ $wizard->limit() }}</span>

                    @if ($wizard->hasNext())
                        <div class="row ">
                            <div class="col-xs-12 payment-bt">
                                <button class="subscribe btn btn-success btn-lg btn-block" type="submit">Continue</button>
                            </div>
                        </div>

                    @else
                        <div class="row ">
                            <div class="col-xs-12 payment-bt">
                                <button class="subscribe btn btn-success btn-lg btn-block" type="button">Done</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
        </div>

    </div>

@endsection


{{--<ol>
    @foreach($wizard->all() as $key => $_step)
        <li>
            @if($step->index == $_step->index)
                <strong>{{ $_step::$label }}</strong>
            @elseif($step->index > $_step->index)
                <a href="{{ route('wizard.user', [$_step::$slug]) }}">{{ $_step::$label }}</a>
            @else
                {{ $_step::$label }}
            @endif
        </li>
    @endforeach
</ol>--}}

