<fieldset>
    <div>
        <h4>Credit/Debt Card</h4>
    </div>

    @if ($errors->any())
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>

    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="form-field">
                <label>Card Number</label>
                <div class="form-field cardNumber">
                    <input type="tel" name="cardNumber" placeholder="Valid Card Number" required />
                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-7">
            <div class="form-field">
                <label><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">EXP</span> Date</label>
                <input type="tel" name="cardExpiry" placeholder="MM / YY" required />
            </div>
        </div>
        <div class="col-xs-12 col-md-5 pull-right">
            <div class="form-field">
                <label>CV Code</label>
                <input type="tel" name="cardCVC" placeholder="CVC" required />
            </div>
        </div>
    </div>

<div class="row">
    <h4>Bank Details</h4>

    <div class="col-md-12">
        <div class="form-field">
            <label>Bank name <span class="red">*</span></label>
            <input type="text" name="bank_name" placeholder="Enter your bank name" />

        </div>
    </div>
    <div class="col-md-12">
        <div class="form-field">
            <label>Bank Account Number <span class="red">*</span></label>
            <input type="text" name="legal_name" placeholder="Enter your bank account number" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-field">
            <label>Swift Number <span class="red">*</span></label>
            <input type="text" name="legal_name" placeholder="Enter your bank's swift number" />
            <small>You can leave it blank if you don't know</small>
        </div>
    </div>
</div>

</fieldset>
