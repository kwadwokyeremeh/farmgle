@extends('layouts.main')

@section('body-class')
    shopping-cart checkout
@endsection
@section('main-content')


    <div class="section bottom-grass wow fadeIn">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        @hasrole('manufacturer')
                        <h2>Manufacturer</h2>
                        @else
                            <h2>Farmer</h2>
                        @endhasrole
                    </div>

                    <div class="checkout-form">
                <form action="{{ route('wizard.seller.post', [$step::$slug]) }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    @include($step::$view, compact('step', 'errors'))

                    @if ($wizard->hasPrev())
                        <a href="{{ route('wizard.seller', ['step' => $wizard->prevSlug()]) }}">Back</a>
                    @endif

                    <span>Step {{ $step->number }}/{{ $wizard->limit() }}</span>

                    @if ($wizard->hasNext())
                        <div class="row ">
                            <div class="col-xs-12 payment-bt">
                                <button class="subscribe btn btn-success btn-lg btn-block" type="submit">Continue</button>
                            </div>
                        </div>

                    @else
                        <div class="row ">
                            <div class="col-xs-12 payment-bt">
                                <button class="subscribe btn btn-success btn-lg btn-block" type="submit">Done</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end section -->
        </div>

    </div>

@endsection



@section('custom-script')


    <script src="{{asset('js/intlTelInput.min.js')}}"></script>
    <script src="{{asset('js/utils.js')}}"></script>
    <script>
        /*var input = document.querySelector("#phone");
        window.intlTelInput(input);*/

        var input = document.querySelector("#phone"),
            errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = [ "Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];


        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);

        // initialise plugin
        var iti = window.intlTelInput(input, {
            hiddenInput: "full_phone",
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "{{asset('js/utils.js')}}"
        });
    </script>



@endsection

{{--<ol>
    @foreach($wizard->all() as $key => $_step)
        <li>
            @if($step->index == $_step->index)
                <strong>{{ $_step::$label }}</strong>
            @elseif($step->index > $_step->index)
                <a href="{{ route('wizard.user', [$_step::$slug]) }}">{{ $_step::$label }}</a>
            @else
                {{ $_step::$label }}
            @endif
        </li>
    @endforeach
</ol>--}}

