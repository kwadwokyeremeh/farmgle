<?php
/**
 * Created by PhpStorm.
 * User: kyerematics
 * Date: 5/2/19
 * Time: 2:31 PM
 */

namespace Farmgle\Modules\Registration\ServiceOriented;

use Farmgle\Freelancer;
use Farmgle\Institution;
use Farmgle\Modules\Profile;
use Illuminate\Support\Facades\Auth;
use Smajti1\Laravel\Step;
use Illuminate\Http\Request;


/**
 *  @mixin \Eloquent
 * */

class VisionMissionStatement extends Step
{

    public static $label = 'Vision and Mission Statement';
    public static $slug = 'vision_n_mission_statement';
    /**
     * @$view resources/views
     * @var string
     */
    public static $view = 'registration.serviceOriented.2';

    public function process(Request $request)
    {

        $user = Auth::user();
       /* if ($user->hasRole('professional')){
            $userType = Freelancer::findOrFail($user)->first();

        }
        else{
            $userType = Institution::findOrFail($user)->first();
        }*/


        $profile = Profile::firstOrNew([
            'who_you_are'       =>$request['description'],
            'mission'       =>$request['mission'],
            'vision'        =>$request['vision'],
            'your_why'      =>$request['your_why'],
            //'profileable_id'=>$userType->id,
            //'profileable_type'=>get_class($userType),
        ]);


        $user->userType()->profile()->save($profile);

        $this->saveProgress($request);
    }

        public function rules(Request $request = null): array
    {

        return [
            'description' => 'required|string|min:50',
            'mission'     => 'required|string|min:50',
            'vision'      => 'required|string|min:50',
            'your_why'    => 'required|string|min:50',
        ];
    }


/*
 * if you want to pass any additional information to the view
 * */
    public function customizeData()
    {

    }

}

