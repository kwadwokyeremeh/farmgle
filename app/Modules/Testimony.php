<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Testimony
 *
 * @property int $id
 * @property int $commentable_id
 * @property string $commentable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Testimony whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Testimony extends Model
{

}
