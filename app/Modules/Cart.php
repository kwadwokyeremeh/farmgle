<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;

/**
 * Farmgle\Modules\Cart
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\Cart whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cart extends Model
{
    //
}
