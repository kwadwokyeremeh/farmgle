<?php

namespace Farmgle\Modules;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Farmgle\Modules\ImageConverter
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Modules\ImageConverter whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ImageConverter extends Model implements HasMedia
{
    //
    use HasMediaTrait;

    protected $table = 'image_converter';

    protected $fillable = [
        'name', 'path'
    ];

    protected $width = 1024;
    protected $height = 768;

    public $registerMediaConversionsUsingModelInstance = true;

    public function registerMediaCollections()
    {
        $this->addMediaCollection('images')
            ->registerMediaConversions(function (\Spatie\MediaLibrary\Models\Media $media = null)
            {
                $this->addMediaConversion('thumb')
                   ->width($this->width)
                    ->height($this->height)
                    ->background('transparent')
                    ->devicePixelRatio(1)
                    ->quality(90)
                    ->format('gif');
            });

    }

    public function mediaAssociation()
    {
        return $this->morphMany(Media::class, 'associable');
    }


}
