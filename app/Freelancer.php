<?php

namespace Farmgle;

use Farmgle\Modules\Follower;
use Farmgle\Modules\Post;
use Farmgle\Modules\Service;
use Farmgle\Modules\Profile;
use Farmgle\Modules\SocialMediaHandlers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Farmgle\Modules\Media as Association;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasPermissions;

/**
 * Farmgle\Freelancer
 *
 * @property int $id
 * @property string $profession
 * @property string|null $email
 * @property string|null $phone
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string|null $certificate
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Farmgle\Modules\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Service[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\SocialMediaHandlers[] $socialMediaHandlers
 * @property-read \Farmgle\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereProfession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereUserId($value)
 * @mixin \Eloquent
 * @property float|null $latitude
 * @property float|null $longitude
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer whereLongitude($value)
 * @property-read \Farmgle\Identity $identity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Media[] $mediaAssociation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Follower[] $follows
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Farmgle\Modules\Post[] $posts
 * @method static \Illuminate\Database\Eloquent\Builder|\Farmgle\Freelancer permission($permissions)
 */
class Freelancer extends Model implements HasMedia
{
    use HasMediaTrait,HasPermissions;

    protected $fillable =[
        'profession',
        'email'  ,
        'phone'  ,
        'address',
        'city'   ,
        'state'  ,
        'country',
        'latitude',
        'longitude',

    ];




    /**
     * The attributes that should freelancer's permission.
     *
     * @var array
     */
    protected $permissions = [
        'create-blog',
        'update-blog',
        'delete-blog',
        'read-blog',
        'create-profile',
        'update-profile',
        'read-profile',
        'delete-profile',
        'create-comment',
        'update-comment',
        'read-comment',
        'delete-comment',
        'read-product',
        'buy-product',
        'read-auction',
        'bid-auction',
    ];


    /*
     * Get the institution  belonging to the user
     * */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /*Freelancer has many social media handlers
    * Get all freelancer's social media accounts
    */
    public function socialMediaHandlers(){
        return $this->morphMany(SocialMediaHandlers::class, 'sociable');
    }


    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }

    public function services()
    {

        return $this->morphMany(Service::class, 'serviceable');
    }

    public function identity()
    {
        return $this->morphOne(Identity::class, 'identifiable');
    }


    /*
     * This returns the models this user follows
     * */
    public function follows()
    {
        return $this->morphMany(Follower::class,'followable');
    }

    /*
     * This method is responsible for storing all media for safe keep
     * */
    public function mediaAssociation()
    {
        return $this->morphMany(Association::class, 'associable');
    }


    public function registerMediaCollections()
    {

        $this
            ->addMediaCollection('listing')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('thumb-1000')
                    ->width(1000)
                    ->height('650');

                $this
                    ->addMediaConversion('thumb-577')
                    ->width(577)
                    ->height(375);

                $this
                    ->addMediaConversion('thumb-258')
                    ->width(258)
                    ->height(167);

                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });

        $this
            ->addMediaCollection('cover-page')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('portrait')
                    ->width(592)
                    ->height(437);

                $this
                    ->addMediaConversion('landscape')
                    ->width(365)
                    ->height(590);

                $this
                    ->addMediaConversion('small')
                    ->width(278)
                    ->height(299);
            });

        /*
         * This image collection is for singled out images
         *
         * */
        $this
            ->addMediaCollection('services')
            ->registerMediaConversions(function (Media $media = null){
                $this
                    ->addMediaConversion('why_us')
                    ->width(396)
                    ->height(396);

                $this
                    ->addMediaConversion('our_profile')
                    ->width(650)
                    ->height(700);

                $this
                    ->addMediaConversion('mission')
                    ->width(874)
                    ->height(603);

                $this
                    ->addMediaConversion('appointment')
                    ->width(710)
                    ->height(644);
            });


    }




    public function posters()
    {
        return $this->morphMany(Post::class,'postable');
    }


    /*
     * Scope of a query only to include published freelancers
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     *
     * */

    /*public function scopePublished($query)
    {

        if (App::environment() =='local'){
            $publish = false;
        }else{
            $publish = true;
        }
        return $query->where('published','=',$publish);
    }*/
}
