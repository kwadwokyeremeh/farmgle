<?php

namespace Farmgle\Providers;

use Farmgle\Modules\Testimony;
use Farmgle\Services\CurrencyConverter2;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Swap\Service\Registry;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(191);

        Validator::extend('uniquePhone', function ($attribute, $value, $parameters, $validator) {
            $count = DB::table('users')->where('phone', $value)
                /*->where('lastName', $parameters[0])*/
                ->count();

            return $count === 0;
        });
        Relation::morphMap([
            'testimony' => Testimony::class
        ]);

        /*
         * Register currency converter
         * */
        Registry::register('currency_converter_2',CurrencyConverter2::class);
    }
}
