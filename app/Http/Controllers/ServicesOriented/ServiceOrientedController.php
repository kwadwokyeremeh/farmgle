<?php

namespace Farmgle\Http\Controllers\ServicesOriented;

use Farmgle\Freelancer;
use Farmgle\Identity;
use Farmgle\Institution;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class ServiceOrientedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $freelancers = Freelancer::with(['identity','media','profile','services','socialMediaHandlers'])->get();
        $institutions = Institution::with(['identity','media','profile','services','socialMediaHandlers'])->get();

        $allServices = (new Collection(Arr::flatten([$freelancers, $institutions])))->shuffle();

       /* foreach ($allServices as $allService) {
            if (Arr::has($allService,'profession')){
                $return = true;
            }else{
                $return= false;
            }
            dd($allServices,$allService,$return);
        }*/
        //dd($allServices);
        return view('serviceOriented.all_services',compact('allServices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @route {services?}
     * @param  uuid $services
     * @return \Illuminate\Http\Response
     */
    public function show($services)
    {
        $identity = Identity::whereIdentifier($services)->firstOrFail();
        $identifiable = $identity->identifiable->firstOrFail();

        return view('serviceOriented.theme2',compact($identifiable));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
