<?php

namespace Farmgle\Http\Controllers;

use Farmgle\Freelancer;
use Farmgle\Institution;
use Farmgle\Modules\Post;
use Farmgle\Modules\Product;
use Farmgle\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class TimelineController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array1 = [];
        $following = Auth::user()->followers()->get();
        foreach ($following as $followee){
            if (get_class($followee->followable) !== User::class){
                array_push($array1,
                    $followee->followable
                        ->with(['identity','media','profile','services','socialMediaHandlers'])
                        ->get());
            }
            //Uncomment out only if there is a use of the users on timeline
            /*else{
                array_push($array1,$followee->followable);
            }*/

        }
        $posts = Post::all();
        $products = Product::with('media')->get();

        /**
         * This is for ads
         *
         * */
        //$freelancers = Freelancer::with(['identity','media','profile','services','socialMediaHandlers'])->get();
       // $institutions = Institution::with(['identity','media','profile','services','socialMediaHandlers'])->get();
        /**
         * This is for ads
         *
         * */
        $userTimeline = (new Collection(Arr::flatten([$array1, $products, $posts])))
            ->shuffle()->sortByDesc('updated_at');

        return view('timeline.index',compact('userTimeline'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
