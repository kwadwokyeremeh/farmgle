<?php

namespace Farmgle\Http\Controllers;

use Farmgle\Modules\ImageConverter;
use Farmgle\Modules\Media;
use Illuminate\Http\Request;

class ImageConverterController extends Controller
{
    public function index()
    {
        return view('image_converter');
    }

    public function store(Request $request)
    {
        $files = [];
        $names = [];
        $media = $request->file('images');
        foreach ($media as $item) {
            array_push($files, new Media([
                //'name' => $item->getClientOriginalName(),
                'path'  => $item->storeAs('default/images',$item->getClientOriginalName(),'public'),
            ]));
        }

        $converter =(new ImageConverter)::create([
            'name'  => 'ImageConverter',
            'path'  => 'I dont care',
        ]);

        $converter->mediaAssociation()->saveMany($files);
        foreach ($files as $file) {

            $converter->addMedia('storage/'.$file['path'])->toMediaCollection('images');
        }

return redirect()->back();

    }
}
