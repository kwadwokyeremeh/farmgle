<?php

namespace Farmgle\Http\Controllers\Registration;

use Farmgle\Modules\Registration\Seller\AddPayment;
use Farmgle\Modules\Registration\Seller\AddProduct;
use Farmgle\Modules\Registration\Seller\AddProfile;
use Farmgle\Modules\Registration\Seller\TermsAndConditions;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Smajti1\Laravel\Exceptions\StepNotFoundException;
use Smajti1\Laravel\Wizard;

class SellerController extends Controller
{
    public $steps = [
        'seller' => TermsAndConditions::class,
                    AddProfile::class,
                    AddPayment::class,
                    AddProduct::class,


    ];

    protected $wizard;

    public function __construct()
    {
        $this->middleware('auth');
        $this->wizard = new Wizard($this->steps, $sessionKeyName = 'user');
    }

    public function wizard($step = null)
    {
        try {
            if (is_null($step)) {
                $step = $this->wizard->firstOrLastProcessed();
            } else {
                $step = $this->wizard->getBySlug($step);
            }
        } catch (StepNotFoundException $e) {
            abort(404);
        }

        return view('registration.seller.base', compact('step'));
    }

    public function wizardPost(Request $request, $step = null)
    {

        try {
            $step = $this->wizard->getBySlug($step);
        } catch (StepNotFoundException $e) {
            abort(404);
        }

        Validator::make($request->all(),$step->rules($request))->validate();
        //$this->validate($request, $step->rules($request));
        $step->process($request);


        if ($step->number == $this->wizard->limit() ?? true){
            return redirect()->home();
        }
        return redirect()->route('wizard.seller', [$this->wizard->nextSlug()]);
    }
}
