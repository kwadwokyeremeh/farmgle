<?php

namespace Farmgle\Http\Controllers\Registration;

use Farmgle\Http\Middleware\CriterionCheck;
use Farmgle\User;
use Illuminate\Http\Request;
use Farmgle\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CriterionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth',CriterionCheck::class]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('criterion');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),['user_type' => 'string'])->validate();


        /* Remove all user roles and permissions*/
        /*if (auth()->user()->hasRole(Role::all())){
            foreach (Role::all() as $item){
                $user = auth()->user();
                $user->removeRole($item);

            }

        }*/
        $user = Auth::user();
        $userType =  $request['user_type'];
        $role = Role::firstOrCreate(['name' => $userType]);
        $user->assignRole($role);

        if ($userType == 'farmer'){
            $user->createFarmerPermissions($role);

            return redirect()->route('wizard.seller');

        }
        elseif ($userType == 'manufacturer'){
            $user->createManufacturerPermissions($role);

            return redirect()->route('wizard.seller');
        }
        elseif ($userType == 'professional'){
            $user->createFreelancerPermissions($role);

            return redirect()->route('wizard.serviceOriented');
        }
        elseif ($userType == 'institution'){
            $user->createInstitutionPermissions($role);

            return redirect()->route('wizard.serviceOriented');

        }
        elseif ($userType == 'logistics'){
            $user->createLogisticsPermissions($role);
            return redirect()->route('wizard.logistics');
        }
        else {
            $user->createCustomerPermissions($role);
            return redirect()->route('home');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
