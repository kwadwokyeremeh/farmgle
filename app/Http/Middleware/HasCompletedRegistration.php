<?php

namespace Farmgle\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasCompletedRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->userType()){
            //
        }
        return $next($request);
    }
}
